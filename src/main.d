import common;

import std.stdio: writeln, writefln;
import std.string: format;
import std.file: exists, isDir, isFile, getcwd, write;
import std.zlib: compress;
import std.path: asAbsolutePath, buildPath, baseName;
import std.array: array;

import dwst.argparse;
import nstall.pkg;

int main(string[] args)
{
	if (args.length < 2)
	{
		writeln("ns-build: missing operand");
		writeln("Try 'ns-build --help' for more information.");
		return 1;
	}

	Opts opts;
	bool ARGHELP, SILENT, DESTINATION;
	string source;
	string destination;
	string[] destarg;
	opts.register("-h", "--help", &ARGHELP);
	opts.register("-s", "--silent", &SILENT);
	opts.register("-o", "--output", &DESTINATION, 1, &destarg);

	try args.parse(opts);
	catch (NotEnoughSubArgs e)
	{
		writeln(e.msg);
		return -1;
	}

	if (ARGHELP)
	{
		writeln(LONGHELP);
		return 0;
	}

	if (DESTINATION)
		destination = destarg[0];
	else
		destination = buildPath(getcwd(), args[$-1]).baseName ~ ".wtar.gz";

	source = args[$-1].asAbsolutePath().array;

	CreatePackage(source, destination, SILENT);
	return 0;
}
