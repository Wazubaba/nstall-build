module nsbuild.common;

string SHORTHELP=
"Usage: ns-build [OPTIONS] path-to-directory";

string LONGHELP=
"Usage: ns-build [OPTION] SOURCE
Generate a .wtar.gz package from SOURCE for use with the Nstall package manager.

[DEST] is defaulted to be 'SOURCE.wtar.gz'

Mandatory arguments to long options are mandatory for short options too.
	-h, --help          show this help
	-o, --output        specify an alternative output file name
	-s, --silent        do not output any operational information";

struct PackageInfo
{
	wstring desc;
	wstring info;
}
